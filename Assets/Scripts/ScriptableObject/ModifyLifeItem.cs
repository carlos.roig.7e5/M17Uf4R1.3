using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ModifyItemData", menuName = "ModifyItemData")]
public class ModifyLifeItem : ItemData
{
    public float LifeModificator;
}
